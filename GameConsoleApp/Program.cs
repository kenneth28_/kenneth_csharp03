﻿using System;

namespace GameConsoleApp
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Welcome To ConsoleApp RPG");
            Console.Write("Your Player Name : ");
            player playerid = new player();
            string weaponselected = " ";
            string playername = Console.ReadLine();
            string[] weapons = { "(1) Dual Sword", "(2) Longbow", "(3) Spear" };
            Console.WriteLine("");
            Console.WriteLine("Select Your Weapon : ");
            foreach (string i in weapons)
            {
                Console.WriteLine(i);
            }
            string weaponid = Console.ReadLine();
            weaponselected = "Dual Sword";
            if (weaponid != null)
            {
                if (weaponid == "1")
                {
                    playerid.weapon = new dual_sword();
                    weaponselected = "Dual Sword (5000)";
                }

                else if (weaponid == "2")
                {
                    playerid.weapon = new longbow();
                    weaponselected = "Long Bow (3000)";
                }
                else if (weaponid == "3")
                {
                    playerid.weapon = new spear();
                    weaponselected = "Spear (2000)";
                }
                else
                {
                    Console.WriteLine("Please Select A Weapon from the list : ");
                }
            }

            playerid.name = playername;
  
            playerid.welcome();


            playerid.DealDamage();




        }
    }
}
