﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GameConsoleApp
{
    public class player 
    {
        public string name { get; set; }
        public weapon weapon { get; set; }

        public int monsterhp = 10000;
        public int turns = 0;

        public void welcome()
        {
            Console.WriteLine("Welcome To the world : " + name + ", you had been equip with weapon " + weapon.name);
            Console.WriteLine("Monster with " + monsterhp + " HP appeared, ");
        }

        public void DealDamage()
        {
            while (monsterhp > 0)
            {
                monsterhp = monsterhp - weapon.damage;
                turns++;
            }
            Console.WriteLine( name + " had defeated the monster in " + turns + " turns ");

        }



    }
}
